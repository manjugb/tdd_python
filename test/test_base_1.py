from calculus.base_1 import Calculation
from twisted.trial import unittest


class CalculationTestCase(unittest.TestCase):
    def test_add(self):
        calc = Calculation()
        result = calc.add(0,0)
        result_01 = calc.add(4,5)
        self.assertEqual(result, None)
        self.assertEqual(result_01,9)

    def test_subtract(self):
        calc = Calculation()
        result = calc.subtract(7, 3)
        self.assertEqual(result, 4)
        result_01 = calc.subtract(3, 9)
        self.assertEqual(result_01,6)

    def test_multiply(self):
        calc = Calculation()
        result = calc.multiply(12, 5)
        self.assertEqual(result, 60)

    def test_divide(self):
        calc = Calculation()
        result = calc.divide(12, 4)
        self.assertEqual(result, 3)

    def test_swapwt(self):
        calc = Calculation()
        result = calc.swapWTemp(4, 5)
        expresult = 5, 4
        assert result == expresult

    def test_swaptemp(self):
        calc = Calculation()
        result = calc.swapTemp(7, 8)
        expresult = 8, 7
        assert result == expresult

    def test_count(self):
        calc = Calculation()
        result = calc.count(1,5)
        expresult = 1
        assert result == expresult

  