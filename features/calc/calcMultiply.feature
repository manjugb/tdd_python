# -- FILE: features/example.feature
@unit
Feature: Validate

  @Subtract
  Scenario: Validate user able to Subtract and verify with injected value
    Given we have behave installed
    Then Multiply 2 and 3 is 6
    Then Multiply 4 and 1 is 4
    Then Multiply 2 and 10 is 20
    #behave -f allure_behave.formatter:AllureFormatter -o allure/results ./features


   @Subtract
   Scenario Outline: user able to validate Subtract with two variables
    Given we have behave installed
    Then Multiply "<num1>" and "<num2>" is "<result>"

    Examples:
    |num1|num2|result|
    |4   |6   |24  |
    |10  |3   |30   |
    |2   |3   |6  |
    |8   |10  |80   |