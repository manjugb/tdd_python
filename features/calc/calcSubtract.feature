# -- FILE: features/example.feature
@unit
Feature: Validate

  @Subtract
  Scenario: Validate user able to Subtract and verify with injected value
    Given we have behave installed
    Then Subtract 2 and 3 is 6
    Then Subtract 4 and 1 is 3
    Then Subtract 2 and 10 is 8
    #behave -f allure_behave.formatter:AllureFormatter -o allure/results ./features


   @Subtract
   Scenario Outline: user able to validate Subtract with two variables
    Given we have behave installed
    Then Subtract "<num1>" and "<num2>" is "<result>"

    Examples:
    |num1|num2|result|
    |4   |6   |2   |
    |10  |3   |7   |
    |2   |3   |1   |
    |8   |10  |2   |