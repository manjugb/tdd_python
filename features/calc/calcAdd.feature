# -- FILE: features/example.feature
@unit
Feature: Validate Add Feature

  Scenario: Validate user able to add and verify with injected value
    Given we have behave installed
    Then add 2 and 3 is 6



  Scenario Outline: user able to validate add with two variables
    Given we have behave installed
    Then add "<num1>" and "<num2>" is "<result>"

    Examples:
    |num1|num2|result|
    |4   |6   |10   |
    |10  |3   |10   |
    |2   |3   |10   |
    |8   |10  |18   |

Scenario Outline: user able to validate Subtract with two variables
    Given we have behave installed
    Then Subtract "<num1>" and "<num2>" is "<result>"

    Examples:
    |num1|num2|result|
    |4   |6   |2   |
    |10  |3   |7   |
    |2   |3   |1   |
    |8   |10  |2   |