


class Calculation(object):

    def add(self, a, b):
        if a > 0 and b > 0:
            return a + b
        else:
            print("values should be greater than zero")

    def subtract(self, a, b):
        if a > b:
            return a - b
        else:
            return b - a

    def multiply(self, a, b):
        return a * b

    def divide(self, a, b):
        return a // b

    @staticmethod
    def swapTemp(i, j):
        tmp = i
        i = j
        j = tmp
        return i, j

    @staticmethod
    def swapWTemp(i, j):
        i, j = j, i
        return i, j

    @staticmethod
    def count(v,j):
        z = int(v/j)
        print(z)
        if v % j == 0:
            return z
            print(z)
        else:
            return z+1
            print(z+1)



